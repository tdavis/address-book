# PagerDuty Address Book with re-frame

A [re-frame](https://github.com/day8/re-frame) implementation of the
PagerDuty "Address book" exercise. 

The re-frame project is an excellent example of **Coherent Software**. Its
[documentation](https://day8.github.io/re-frame/a-loop/) presents a clear,
concise, and comprehensible narrative for its design *and* implementation. The
patterns used by re-frame are well-argued and its APIs are elegant. Most
end-user operations are simple to implement, rarely requiring more than a
function call. The re-frame project successfully encapsulates complex ideas
within a simple API that optimizes for the success of re-frame users by
providing patterns that trend toward maintainability.

## Setup And Run A Development Environment

1. Install [Node.js](https://nodejs.org/en/)

2. Get the re-frame repo from GitHub:
   ```sh
   git clone https://gitlab.com/tdavis/address-book.git
   ```
3. Kick off a clean build (compile the app and start up shadow-cljs hot-reloading)
   ```sh
   npm install
   npm run watch
   ```

4. Wait for the compile in step 4 to finish. At a minumum, 15 seconds. But, if you are new to ClojureScript and some downloads are needed (caches are empty), it might take a minute or two. Eventually you should see `[:client] Build Completed (...)`

5. Wait for step 4 to do the compile, and then open in UI in the broswer:
   ```sh
   open http://localhost:8280
   ```

## Exploring The Code

From the re-frame docs:
```
To build a re-frame app, you:
  - design your app's data structure (data layer)
  - write and register subscription functions (query layer)
  - write Reagent component functions (view layer)
  - write and register event handler functions (control layer and/or state transition layer)
  - once in a blue moon you need to write effect handlers
```

In `src`, there's a matching set of files (each small):
```
src
├── core.cljs         <--- entry point, plus history
├── db.cljs           <--- data related  (data layer)
├── subs.cljs         <--- subscription handlers  (query layer)
├── views.cljs        <--- reagent  components (view layer)
└── events.cljs       <--- event handlers (control/update layer)
```

For the most immediate feedback, edit some of the hiccup in the `views.cljs` file. When 
you save the file, shadow-cljs will immediately compile your changes and hot-reload them into the browser. 

## To Compile An Optimised Version

1. Compile
   ```sh
   npm install
   npm run release
   ```

2. Open the following in your browser
   ```sh
   resources/public/index.html
   ```
