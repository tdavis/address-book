(ns addy.subs
  (:require [re-frame.core :refer [reg-sub subscribe]]))


;; -------------------------------------------------------------------------------------
;; Layer 2
;;
;; See https://day8.github.io/re-frame/subscriptions/
;;
;; Layer 2 query functions are "extractors". They take from `app-db`
;; and don't do any further computation on the extracted values. Any further
;; computation should happen in Layer 3.
;; Why?  It is an efficiency thing. Every Layer 2 subscription will rerun any time
;; that `app-db` changes (in any way). As a result, we want Layer 2 to be trivial.
;;
(reg-sub
 :showing          ;; usage:   (subscribe [:showing])
 (fn [db _]        ;; db is the (map) value stored in the app-db atom
   (:showing db))) ;; extract a value from the application state

(reg-sub
 :sorted-users     ;; usage: (subscribe [:sorted-users])
 (fn
   [db _]
   (:users db)))

(reg-sub
 :more?            ;; usage: (subscribe [:more?])
 (fn
   [db _]
   (:more? db)))


;; -------------------------------------------------------------------------------------
;; Layer 3
;;
;; See https://day8.github.io/re-frame/subscriptions/
;;
;; A subscription handler is a function which is re-run when its input signals
;; change. Each time it is rerun, it produces a new output (return value).
;;
;; In the simple case, app-db is the only input signal, as was the case in the two
;; simple subscriptions above. But many subscriptions are not directly dependent on
;; app-db, and instead, depend on a value derived from app-db.
;;
;; Such handlers represent "intermediate nodes" in a signal graph.  New values emanate
;; from app-db, and flow out through a signal graph, into and out of these intermediate
;; nodes, before a leaf subscription delivers data into views which render data as hiccup.
;;
;; When writing and registering the handler for an intermediate node, you must nominate
;; one or more input signals (typically one or two).
;;
;; reg-sub allows you to supply:
;;
;;   1. a function which returns the input signals. It can return either a single signal or
;;      a vector of signals, or a map where the values are the signals.
;;
;;   2. a function which does the computation. It takes input values and produces a new
;;      derived value.
;;
;; In the two simple examples at the top, we only supplied the 2nd of these functions.
;; But now we are dealing with intermediate (layer 3) nodes, we'll need to provide both fns.
;;
(reg-sub
 :users        ;; usage:   (subscribe [:users])

 ;; This function returns the input signals.
 ;; Although not required in this example, it is called with two parameters
 ;; being the two values supplied in the originating `(subscribe X Y)`.
 ;; X will be the query vector and Y is an advanced feature and out of scope
 ;; for this explanation.
 (fn [query-v _]
   [(subscribe [:sorted-users])
    (subscribe [:showing])])

 ;; This 2nd fn does the computation. Data values in, derived data out.
 ;; It is the same as the two simple subscription handlers up at the top.
 ;; Except they took the value in app-db as their first argument and, instead,
 ;; this function takes the value delivered by another input signal, supplied by the
 ;; function above: (subscribe [:sorted-users])
 ;;
 ;; Subscription handlers can take 3 parameters:
 ;;  - the input signals (a single item, a vector or a map)
 ;;  - the query vector supplied to query-v  (the query vector argument
 ;; to the "subscribe") and the 3rd one is for advanced cases, out of scope for this discussion.
 (fn [[sorted-users showing] query-v _]
   (map
    (fn [[id user]] (assoc user :id id :showing? (= showing id)))
    (seq sorted-users))))

;; -------------------------------------------------------------------------------------
;; Hey, wait on!!
;;
;; How did those two simple Layer 2 registrations at the top work?
;; We only supplied one function in those registrations, not two?
;; Very observant of you, I'm glad you asked.
;; When the signal-returning-fn is omitted, reg-sub provides a default,
;; and it looks like this:
;;    (fn [_ _]
;;       re-frame.db/app-db)
;; It returns one signal, and that signal is app-db itself.
;;
;; So the two simple registrations at the top didn't need to provide a signal-fn,
;; because they operated only on the value in app-db, supplied as 'db' in the 1st argument.
;;
;; So that, by the way, is why Layer 2 subscriptions always re-calculate when `app-db`
;; changes - `app-db` is literally their input signal.
