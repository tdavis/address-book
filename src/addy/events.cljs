(ns addy.events
  (:require
   [ajax.core       :as ajax]
   [addy.db         :refer [default-db users->local-store]]
   [re-frame.core   :refer [reg-event-db reg-event-fx inject-cofx path after]]
   [cljs.spec.alpha :as s]
   [clojure.string :as str]))


;; -- Interceptors --------------------------------------------------------------
;;
;; Interceptors are a more advanced topic. So, we're plunging into the deep
;; end here.
;;
;; There is a tutorial on Interceptors in re-frame's `/docs`, but to get
;; you going fast, here's a very high level description ...
;;
;; Every event handler can be "wrapped" in a chain of interceptors. A
;; "chain of interceptors" is actually just a "vector of interceptors". Each
;; of these interceptors can have a `:before` function and an `:after` function.
;; Each interceptor wraps around the "handler", so that its `:before`
;; is called before the event handler runs, and its `:after` runs after
;; the event handler has run.
;;
;; Interceptors with a `:before` action, can be used to "inject" values
;; into what will become the `coeffects` parameter of an event handler.
;; That's a way of giving an event handler access to certain resources,
;; like values in LocalStore.
;;
;; Interceptors with an `:after` action, can, among other things,
;; process the effects produced by the event handler. One could
;; check if the new value for `app-db` correctly matches a Spec.
;;


;; -- First Interceptor ------------------------------------------------------
;;
;; Event handlers change state, that's their job. But what happens if there's
;; a bug in the event handler and it corrupts application state in some subtle way?
;; Next, we create an interceptor called `check-spec-interceptor`.
;; Later, we use this interceptor in the interceptor chain of all event handlers.
;; When included in the interceptor chain of an event handler, this interceptor
;; runs `check-and-throw` `after` the event handler has finished, checking
;; the value for `app-db` against a spec.
;; If the event handler corrupted the value for `app-db` an exception will be
;; thrown. This helps us detect event handler bugs early.
;; Because all state is held in `app-db`, we are effectively validating the
;; ENTIRE state of the application after each event handler runs.  All of it.


(defn check-and-throw
  "Throws an exception if `db` doesn't match the Spec `a-spec`."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

;; now we create an interceptor using `after`
(def check-spec-interceptor (after (partial check-and-throw :addy.db/db)))


;; -- Second Interceptor -----------------------------------------------------
;;
;; Part of the TodoMVC challenge is to store users in local storage.
;; Next, we define an interceptor to help with this challenge.
;; This interceptor runs `after` an event handler, and it stores the
;; current users into local storage.
;; Later, we include this interceptor into the interceptor chain
;; of all event handlers which modify users.  In this way, we ensure that
;; every change to users is written to local storage.
(def ->local-store (after #(->> % :users users->local-store)))


;; -- Interceptor Chain ------------------------------------------------------
;;
;; Each event handler can have its own chain of interceptors.
;; We now create the interceptor chain shared by all event handlers
;; which manipulate users.
;; A chain of interceptors is a vector of interceptors.
(def user-interceptors [check-spec-interceptor    ;; ensure the spec is still valid  (after)
                        ->local-store])           ;; write users to localstore  (after)


;; -- Event Handlers ----------------------------------------------------------

;; usage:  (dispatch [:initialise-db])
;;
;; This event is dispatched in the app's `main` (core.cljs).
;; It establishes initial application state in `app-db`.
;; That means merging:
;;   1. Any users stored in LocalStore (from the last session of this app)
;;   2. Default initial values
;;
;; Advanced topic:  we inject the users currently stored in LocalStore
;; into the first, coeffect parameter via use of the interceptor
;;    `(inject-cofx :local-store-users)`
;;
;; To fully understand this advanced topic, you'll have to read the tutorials
;; and look at the bottom of `db.cljs` for the `:local-store-users` cofx
;; registration.
(reg-event-fx                 ;; part of the re-frame API
 :initialise-db              ;; event id being handled

 ;; the interceptor chain (a vector of 2 interceptors in this case)
 [(inject-cofx :local-store-users) ;; gets users from localstore, and puts value into coeffects arg
  check-spec-interceptor]          ;; after event handler runs, check app-db for correctness. Does it still match Spec?

 ;; the event handler (function) being registered
 (fn [{:keys [db local-store-users]} _]                  ;; take 2 values from coeffects. Ignore event vector itself.
   (let [ctx {:db (assoc default-db
                         :users local-store-users
                         :offset (count local-store-users))}]
     (if (empty? local-store-users)
       (assoc ctx :dispatch [:fetch-users])
       ctx))))

;; usage:  (dispatch [:set-showing  "asdf1234"])
;; This event is dispatched when the user clicks on a user row
(reg-event-db
 :set-showing
 [check-spec-interceptor]
 (fn [db [_ user-id]]
   (assoc db :showing user-id)))

;; usage:  (dispatch [:fetch-users])
(reg-event-fx                     ;; given an offset, fetch next page of users
 :fetch-users
 [check-spec-interceptor]
 (fn [{:keys [db]} _]
   (let [offset (:offset db)]
     {:http-xhrio {:method          :get
                   :headers         {"Authorization" "Token token=y_NbAkKc66ryYTWUXYEu"}
                   :uri             "https://api.pagerduty.com/users"
                   :params          {"include[]" "contact_methods"
                                     "offset"    offset
                                     "limit"     10}
                   :format          (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success      [:fetch-users-success]
                   :on-failure      [:fetch-users-failure]}
      :db         (assoc db :offset (+ 10 offset))})))

(defn ->user [res]
  (let [{:keys [name contact_methods]} res
        [first-name last-name] (str/split name #"\s" 2)
        some-type #(if (= (:type %2) (str %1 "_contact_method")) %2)
        sms (some (partial some-type "sms") contact_methods)
        phone (some (partial some-type "phone") contact_methods)
        email (some (partial some-type "email") contact_methods)]
    {:first-name first-name
     :last-name last-name
     :contact {:sms (when sms (int (:address sms)))
               :phone (when phone (int (:address phone)))
               :email (:address email)}}))

(reg-event-db
 :fetch-users-success
 user-interceptors
 (fn [db [_ result]]
   (let [more? (:more result)
         users (->> result :users (reduce #(assoc %1 (:id %2) (->user %2)) {}))]
     (assoc db :more? more? :users (into (:users db) users)))))

(reg-event-db
 :fetch-users-failure
 [check-spec-interceptor]
 (fn [db [_ result]]
   (throw (ex-info (str "users request failed: " result) {}))
   db))
