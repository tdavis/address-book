(ns addy.db
  (:require [cljs.reader]
            [cljs.spec.alpha :as s]
            [re-frame.core :as re-frame]))


;; -- Spec --------------------------------------------------------------------
;;
;; This is a clojure.spec specification for the value in app-db. It is like a
;; Schema. See: http://clojure.org/guides/spec
;;
;; The value in app-db should always match this spec. Only event handlers
;; can change the value in app-db so, after each event handler
;; has run, we re-check app-db for correctness (compliance with the Schema).
;;
;; How is this done? Look in events.cljs and you'll notice that all handlers
;; have an "after" interceptor which does the spec re-check.
;;
;; None of this is strictly necessary. It could be omitted. But we find it
;; good practice.

(def email-regex #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")

(s/def ::id string?)
(s/def ::first-name string?)
(s/def ::last-name (s/nilable string?))
(s/def ::sms (s/nilable number?))
(s/def ::phone (s/nilable number?))
(s/def ::email (s/nilable (s/and string? #(re-matches email-regex %))))
(s/def ::more? boolean?)
(s/def ::offset number?)

(s/def ::contact (s/keys :req-un [::sms ::phone ::email]))
(s/def ::user (s/keys :req-un [::first-name ::last-name ::contact]))
(s/def ::users (s/and (s/map-of ::id ::user) #(instance? PersistentTreeMap %)))
(s/def ::showing (s/nilable string?))
(s/def ::db (s/keys :req-un [::users ::more? ::offset ::showing]))

;; -- Default app-db Value  ---------------------------------------------------
;;
;; When the application first starts, this will be the value put in app-db
;; Unless, of course, there are users in the LocalStore (see further below)
;; Look in:
;;   1.  `core.cljs` for  "(dispatch-sync [:initialise-db])"
;;   2.  `events.cljs` for the registration of :initialise-db handler
;;

(def default-db           ;; what gets put into app-db by default.
  {:users   (sorted-map)  ;; an empty list of users. Use the (int) :id as the key
   :showing nil           ;; not showing any user details
   :more?   true          ;; more uses can be fetched
   :offset  0             ;; current page offset
   })

;; -- Local Storage  ----------------------------------------------------------
;;
;; Part of the todomvc challenge is to store users in LocalStorage, and
;; on app startup, reload the users from when the program was last run.
;; But the challenge stipulates to NOT load the setting for the "showing"
;; filter. Just the users.
;;

(def ls-key "address-book")                          ;; localstore key

(defn users->local-store
  "Puts users into localStorage"
  [users]
  (.setItem js/localStorage ls-key (str users)))     ;; sorted-map written as an EDN map


;; -- cofx Registrations  -----------------------------------------------------

;; Use `reg-cofx` to register a "coeffect handler" which will inject the users
;; stored in localstore.
;;
;; To see it used, look in `events.cljs` at the event handler for `:initialise-db`.
;; That event handler has the interceptor `(inject-cofx :local-store-users)`
;; The function registered below will be used to fulfill that request.
;;
;; We must supply a `sorted-map` but in LocalStore it is stored as a `map`.
;;
(re-frame/reg-cofx
 :local-store-users
 (fn [cofx _]
   ;; put the localstore users into the coeffect under :local-store-users
   (assoc cofx :local-store-users
          ;; read in users from localstore, and process into a sorted map
          (into (sorted-map)
                (some->> (.getItem js/localStorage ls-key)
                         (cljs.reader/read-string)    ;; EDN map -> map
                         )))))
