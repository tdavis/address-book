(ns addy.views
  (:require [reagent.core  :as reagent]
            [re-frame.core :refer [subscribe dispatch]]
            [clojure.string :as str]))


(defn user-item []
  (fn [{:keys [id contact first-name last-name showing?]}]
    [:li {:on-click #(dispatch [:set-showing id])
          :id (when showing? "showing")}
     [:label (str first-name " " (or last-name "?"))]
     (when showing?
       [:div#contact
        (when-let [addr (:email contact)]
          [:p [:label "Email"] addr])
        (when-let [addr (:phone contact)]
          [:p [:label "Phone"] addr])
        (when-let [addr (:sms contact)]
          [:p [:label "SMS"] addr])])]))


(defn user-list []
  (let [users @(subscribe [:users])]
    [:section#main
     [:ul#user-list
      (for [user (seq users)]
        ^{:key (:id user)} [user-item user])]]))


(defn fetch-button []
  (let [more? @(subscribe [:more?])]
    [:div#fetch
     [:button
      {:disabled (when-not more? "disabled")
       :on-click #(dispatch [:fetch-users])}
      "Add More Users"]]))

(defn user-app
  []
  [:<>
   [:section#addy
    [:h1 "Address Book"]
    [fetch-button]
    (when (seq @(subscribe [:users]))
      [user-list])
    ]])
